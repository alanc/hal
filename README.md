This repository contains a "solaris" branch representing the changes to HAL
made in the Solaris code base.

It does not build as is, since the version in Solaris uses the Solaris ON
Makefiles, not autoconf/automake, and effort has not been put in to adapt
the upstream Makefiles to match.

Not all of the changes are appropriate to upstream, or to non-Solaris kernels,
but those that are useful are available under the license terms stated in
the files in question.
